# VR Template for Oculus Go and VRTK 3.3.0

This is a template for VR Unity projects. Please dont edit this repo :)

This project has been set up for the Oculus Go, using the [setup suggested by Oculus](https://developer.oculus.com/blog/tech-note-unity-settings-for-mobile-vr/).

Using: 

 - Unity 2018.2.x 
 - Oculus Intergration 1.35
 - VRTK 3.3.0 